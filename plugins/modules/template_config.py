#!/usr/bin/python

# Copyright: (c) 2018, Terry Jones <terry.jones@example.org>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: parse_deploy_yaml

short_description: This is my test module

# If this is part of a collection, you need to use semantic versioning,
# i.e. the version is of the form "2.5.0" and not "2.4".
version_added: "1.0.0"

description: This is my longer description explaining my test module.

options:
    name:
        description: This is the message to send to the test module.
        required: true
        type: str
    new:
        description:
            - Control to demo if the result of this module is changed or not.
            - Parameter description can be a list as well.
        required: false
        type: bool
# Specify this value according to your collection
# in format of namespace.collection.doc_fragment_name
extends_documentation_fragment:
    - my_namespace.my_collection.my_doc_fragment_name

author:
    - Your Name (@yourGitHubHandle)
'''

EXAMPLES = r'''
# Pass in a message
- name: Test with a message
  my_namespace.my_collection.my_test:
    name: hello world

# pass in a message and have changed true
- name: Test with a message and changed output
  my_namespace.my_collection.my_test:
    name: hello world
    new: true

# fail the module
- name: Test failure of the module
  my_namespace.my_collection.my_test:
    name: fail me
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
original_message:
    description: The original name param that was passed in.
    type: str
    returned: always
    sample: 'hello world'
message:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

from ansible.module_utils.basic import AnsibleModule

from jinja2 import Environment, PackageLoader, select_autoescape, FileSystemLoader



template_env = Environment(loader=FileSystemLoader('/working_dir/'))
# template_env.get_template('foo/bar.html')



def run_module():
    module_args = dict(
        # deployment_spec=dict(type='dict', required=True),
        # test=dict(type='bool', required=True),
        # new=dict(type='bool', required=False, default=False)
    )

    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # AIM: create a file system hierarchy of terraform declarations
    # each provider will receive a folder
    # each resource type will receive a folder





    # for provider in module.params['deployment_spec']['providers']:

        # t = Template('{% macro foo() %}42{% endmacro %}23')
        #
        #
        #
        #
        #
        # for droplet in provider['resources']['droplets']:
        #     key_list = []
        #     droplet_count = 1
        #     if 'count' in droplet:
        #         droplet_count = int(droplet['count'])
        #     for index in range(0, droplet_count):
        #         private_key = ec.generate_private_key(ec.SECP521R1())
        #
        #         public_key = private_key.public_key()
        #
        #         key_list.append({
        #             'private': private_key.private_bytes(
        #                 encoding=serialization.Encoding.PEM,
        #                 format=serialization.PrivateFormat.PKCS8,
        #                 encryption_algorithm=serialization.NoEncryption()
        #             ),
        #             'public': public_key.public_bytes(
        #                 encoding=serialization.Encoding.OpenSSH,
        #                 format=serialization.PublicFormat.OpenSSH,
        #             ),
        #
        #         })
        #     droplet['ssh_keys'] = key_list

    result = {
        "changed": True,
        "failed": False,
        "spec": ""
    }

    module.exit_json(**result)

def main():
    run_module()


if __name__ == '__main__':
    main()

